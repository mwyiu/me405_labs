'''
@file       touchpanel.py

@brief      Touch panel driver
@details    This file contains the Scanner class that initializes a scanner
            object that scans the x, y, and z, positions of the touch panel.
            This driver will be used in a future lab to determine the location
            of the rubber ball while trying to balance it. 4 pins, positive
            and negative x and y. Measuring a voltage across a combination of
            pin setups will allow us to determine the specific positions of
            x, y, and z.
@author     Michael Yiu
@date       March 4, 2021
'''
from pyb import Pin
from pyb import ADC
import utime

class Scanner:
    '''
    @brief      This class is a driver for the resistive touch panel used in ME 405.
    @details    This class scans the x, y, and z-position of the touch panel.
                There are four pins: x_p, x_m, y_p, and y_m. The positions are
                scanned by setting two pins as high and low, one pin as a 
                floating input, and measuring the voltage of remaining pin with
                an analog-to-digital converter. Depending on the configuration,
                the x, y, or z-direction is measured. To use this class, initialize
                the Scanner object by inputting the four pins and the length,
                width, and center of the touch panel. Afterwards, scan the positions
                of the point of contact on the touch panel with any of the class
                functions.
    '''
    def __init__(self, x_p, x_m, y_p, y_m, width, length, center):
        '''
        @brief          Initializes the Scanner object.
        @details        Initializes the Scanner object and copies the parameters
                        of the touch panel, such as its width and length. The
                        Scanner object also copies the pinouts for the 
                        positive and negative x and y pins.
        @param x_p      Pin callout for the positive x pin.
        @param x_m      Pin callout for the negative x pin.
        @param y_p      Pin callout for the positive y pin.
        @param y_m      Pin callout for the negative y pin.
        @param width    The width of the touch panel.
        @param length   The length of the touch panel.
        @param center   The center of the touch panel.
        '''
        ## Copy of negative x pin object
        self.x_m = x_m
        ## Copy of positive y pin object
        self.y_p = y_p
        ## Copy of positive x pin object
        self.x_p = x_p
        ## Copy of negative y pin object
        self.y_m = y_m
        ## Copy of touch panel width
        self.width = width
        ## Copy of touch panel length
        self.length = length
        ## Copy of the touch panel's center
        self.center = center
        ## Conversion factor for ADC counts to x-position
        self.convertX = 88/1775
        ## Conversion factor for ADC counts to y-position
        self.convertY = 50/1600
        ## X-intercept for y=mx+b equation to determine x-position on touch panel
        self.Xint = 88/9
        ## Y-intercept for y=mx+b equation to determine y-position on touch panel
        self.Yint = 375/32
        
    def scanX(self):
        '''
        @brief      Scans the x-position at the point of contact.
        @details    The positive x pin is set to high, and the negative x pin is
                    set to low. The voltage across the y pins is measured using
                    an analog-to-digital converter. This voltage is then
                    converted into the relative x-position.
        '''
        # Configuring pins to measure x-position
        self.x_p.init(mode=Pin.OUT_PP, value=1)
        self.x_m.init(mode=Pin.OUT_PP, value=0)
        self.y_p.init(mode=Pin.IN)
        ## ADC reader for the x-position
        self.x_adc = ADC(self.y_m)
        utime.sleep_us(4) # Waiting 4 microseconds for the settling time
        ## The x-position of the touch panel
        self.x = self.x_adc.read()*self.convertX - self.center[0] - self.Xint
        
    def scanY(self):
        '''
        @brief      Scans the y-position at the point of contact.
        @details    The positive y pin is set to high, and the negative y pin is
                    set to low. The voltage across the x pins is measured using
                    an analog-to-digital converter. This voltage is then
                    converted into the relative y-position.
        '''
        # Configuring pins to measure y-position
        self.y_p.init(mode=Pin.OUT_PP, value=1)
        self.y_m.init(mode=Pin.OUT_PP, value=0)
        self.x_p.init(mode=Pin.IN)
        ## ADC reader for the y-position
        self.y_adc = ADC(self.x_m)
        utime.sleep_us(4) # Waiting 4 microseconds for the settling time
        ## The y-position of the touch panel
        self.y = self.y_adc.read()*self.convertY - self.center[1] - self.Yint
        
    def scanZ(self):
        '''
        @brief      Scans whether or not there is contact.
        @details    The positive y pin is set to high, and the negative x pin is
                    set to low. Either the voltage across the negative y pin
                    or across the positive y pin can be measured. This function
                    measures the voltage across the negative y pin with an
                    analog-to-digital converter. This voltage is then
                    converted into the z-position.
        '''
        # Configuring pins to measure z-position
        self.y_p.init(mode=Pin.OUT_PP, value=1)
        self.x_m.init(mode=Pin.OUT_PP, value=0)
        self.x_p.init(mode=Pin.IN)
        ## ADC reader for the z-position
        self.z_adc = ADC(self.y_m)
        utime.sleep_us(4) # Waiting 4 microseconds for the settling time
        ## The z-position of the touch panel
        self.z = self.z_adc.read()
        if self.z < 4000: # Filtering false contact signals
            ## Boolean that represents whether there is contact or not
            self.z_contact = True
        else:
            self.z_contact = False
    
    def readall(self):
        '''
        @brief      Scans all position: x, y, and z.
        @details    Each position is scanned and is stored in a tuple of size
                    three.
        '''
        self.scanX()
        self.scanY()
        self.scanZ()
        ## The position of the touch panel represented with x, y, and z-position
        self.position = (self.x, self.y, self.z_contact)

# if __name__ == "__main__": # Do this: whenever this code is run, define system parameters
#     ## Width of the touch panel in millimeters
#     width = 176
#     ## Length of the touch panel in millimeters
#     length = 100
#     ## Center of the touch panel in millimeters
#     center = (width/2, length/2)
#     ## Pin object for the positive x pin
#     x_p = Pin.cpu.A7
#     ## Pin object for the negative x pin
#     x_m = Pin.cpu.A1
#     ## Pin object for the positive y pin
#     y_p = Pin.cpu.A6
#     ## Pin object for the negative y pin
#     y_m = Pin.cpu.A0
#     ## Initialzing a scanner object and passing in pin objects
#     panel = Scanner(x_p, x_m, y_p, y_m, width, length, center)
#     ## Benchmarking runtime 
#     time = 0 
#     for n in range(100): 
#         ## Time before running scan
#         start=utime.ticks_us()
#         panel.readall()
#         ## Time elapsed to run scan
#         delta = utime.ticks_diff(utime.ticks_us(), start)
#         time += delta
#     print("The average time, over 100 trials, to read all positions (x, y, and z) is {:.2f} microseconds".format(time/n))
#     print("The last reading is: {:}".format(panel.position))
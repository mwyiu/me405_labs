'''
@file       motor.py

@brief      Motor driver
@details    This file contains the MotorDriver class that initializes a motor
            object that drives the motors attached to the balancing board. The
            motors will be controlled using PWM output and can be shut off at
            any moment with an external interrupt. Currently, the external
            interrupt is set on the Blue input button, but can be modified as
            necessary.
@author     Michael Yiu
@date       March 11, 2021
'''
import pyb

class MotorDriver:
    '''
    @brief      This class implements a motor driver for the ME 405 board.
    @details    This class implements a motor object to control a motor.
                To use this class, initalize the motor object by passing in pins
                1 and 2 and their respective timer and timer channel. Additionally,
                pass in the master nSLEEP pin and nFault pin to control fault
                detection. First enable() the motor and then set_duty() to
                power the motor at the desired PWM output.
    '''
    def __init__ (self, pin_nSLEEP, pin_IN1, pin_IN2, timer, channel):
        '''
        @brief Creates a motor driver by initializing GPIO pins and turning the motor off for safety.
        @param pin_nSLEEP A pyb.Pin object to use as the enable pin.
        @param pin_IN1 A pyb.Pin object to use as the input to half bridge 1 for motor 1.
        @param pin_IN2 A pyb.Pin object to use as the input to half bridge 2 for motor 1.
        @param timer A pyb.Timer object to use for PWM generation on IN1_pin and IN2_pin.
        @param channel Channels for the timer object
        '''
        ## Copying the passed pin_nSLEEP object and initializing it as an output pin
        self.pin_nSLEEP = pyb.Pin(pin_nSLEEP, pyb.Pin.OUT_PP)
        ## Copying the passed pin_IN1 object
        self.pin_IN1 = pyb.Pin(pin_IN1)
        ## Copying the passed pin_IN2 object
        self.pin_IN2 = pyb.Pin(pin_IN2)
        ## Copying the passed timer object
        self.timer = timer
        ## Initializing channel 1 to drive the motor
        self.ch1 = self.timer.channel(channel[0], pyb.Timer.PWM, pin=self.pin_IN1)
        ## Initializing channel 2 to drive the motor
        self.ch2 = self.timer.channel(channel[1], pyb.Timer.PWM, pin=self.pin_IN2)
        ## Fault detection flag
        self.fault_detect = False

    def nFault(self):
        '''
        @brief This method is turns off the motor when the ext. interrupt is triggered.
        '''
        self.fault_detect = True
        self.disable()
        
    def enable(self):
        '''
        @brief This method flips the nSLEEP pin high, allowing the motor to spin.
        '''
        if (self.fault_detect == False):
            self.pin_nSLEEP.high()
        else:
            print("nFAULT triggered. Set fault_detect to False to continue.")

    def disable(self):
        '''
        @brief This method flips the nSLEEP pin low, shutting the motor down completely.
        '''
        self.pin_nSLEEP.low()
        self.ch1.pulse_width_percent(0)
        self.ch2.pulse_width_percent(0)

    def set_duty(self, duty):
        ''' 
        @brief This method sets the duty cycle to be sent to the motor to the given level. 
        @details Positive values cause effort in one direction, negative values in the opposite direction.
        @param duty A signed integer holding the duty cycle of the PWM signal sent to the motor 
        '''
        ## Copying the passed duty cycle value
        self.duty = duty
        if(self.duty > 0):
            self.ch1.pulse_width_percent(self.duty)
            self.ch2.pulse_width_percent(0)
        elif(self.duty < 0):
            self.ch1.pulse_width_percent(0)
            self.ch2.pulse_width_percent(abs(self.duty))
        elif(self.duty == 0):
            self.ch1.pulse_width_percent(self.duty)
            self.ch2.pulse_width_percent(self.duty)

# if __name__ == '__main__':
#     ## Creating the nSLEEP pin for the motor driver
#     pin_nSLEEP = pyb.Pin.cpu.A15
#     ## Creating the IN1 pin for the motor driver
#     pin_IN1 = pyb.Pin.cpu.B4
#     ## Creating the IN2 pin for the motor driver
#     pin_IN2 = pyb.Pin.cpu.B5
#     ## Creating the IN1 pin for the motor driver
#     pin_IN3 = pyb.Pin.cpu.B0
#     ## Creating the IN2 pin for the motor driver
#     pin_IN4 = pyb.Pin.cpu.B1
#     ## Create the timer object used for PWM generation
#     timer = pyb.Timer(3, freq=20000)
#     ## Specifying the timer channels for motor 1
#     channel1 = (1, 2)
#     ## Specifying the timer channels for motor 2
#     channel2 = (3, 4)
#     ## Create a motor object 1 and passing in the pins and timer
#     motor1 = MotorDriver(pin_nSLEEP, pin_IN1, pin_IN2, timer, channel1)
#     ## Create a motor object 2 and passing in the pins and timer
#     motor2 = MotorDriver(pin_nSLEEP, pin_IN3, pin_IN4, timer, channel2)
'''
@file lab4_main.py
@author     Collaboration between Michael Yiu & Ben Bons
@date       February 11, 2021

@brief      This file reads temperatures from sensors and records them in a csv file.
@details    This file initializes a coreTemp object as well as a sensor object to
            read the temperature of the MCP9808 sensor and internal Nucleo sensor.
            Readings are taken once every minute, for eight hours or until the
            user enters Ctrl-C. When completed, the data collected will be 
            written onto a csv file.
'''

import pyb
import utime
from mcp9808 import mcp9808
from stm32coreTemp import coreTemp

## Creating a coreTemp object to read the core temperature of the Nucleo
coreTemp = coreTemp()
## Establishing an I2C Connection
i2c = pyb.I2C(1, pyb.I2C.MASTER)
## Determining the address of the MCP9808 sensor
addr = i2c.scan()
## Initalizing the MCP9808 sensor to send data to the Nucleo
sensor = mcp9808(i2c, addr[0])
## Setting the time datum of the data collection
time_start = utime.ticks_ms()

with open ("test.csv", "w") as csv: #Creating a csv file if it does not exist, or overwriting it if it does exist
    csv.write("Time (ms),CoreTemp (C),MCP9808 temp (C),MCP9808 temp (T)\r")
while True:
    # Do this: forever until 8 hours have passed, or the user enters Ctrl-C to stop
    try:
        with open ("test.csv", "a") as csv: #Append the csv file with the time, core temp, and sensor temp in C and F
            csv.write("{:},{:.2f},{:.2f},{:.2f}\r".format((utime.ticks_diff(utime.ticks_ms(), time_start)),coreTemp.getTemp(),sensor.celsius(),sensor.fahrenheit()))
        utime.sleep(60) #Sleep, and thereby collect data in one minute intervals
        if (utime.ticks_diff(utime.ticks_ms(), time_start)) >= 8*60*60*1000:
            # Do this: break the loop if eight hours have passed
            break
    except KeyboardInterrupt:
        # Exit the loop if the user enters Ctrl-C
        break    
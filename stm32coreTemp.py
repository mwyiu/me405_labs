'''
@file stm32coreTemp.py

@brief      This file contains the class that reads the ADC core temperature
@details    This file initializes a coreTemp object to read the Nucleo's
            internal temperature by measuring it from an ADC channel.
@author     Collaboration between Michael Yiu & Ben Bons
@date       February 11, 2021
'''

import pyb

class coreTemp:
    '''
    @brief      This class uses an ADCAll object to read from the Nucleo's internal temperature sensor
    @details    The class reads from the ADC channel connected to the microcontroller's
                internal sensor. The sensor is not particularly accurate, which
                is why an external sensor should be used. To use this class,
                initialize it as an object in the desired file.
    '''
    def __init__(self):
        '''
        @brief Initializes the coreTemp object to read from the ADC
        '''
        ## The ADCAll object that is read from
        self.adc = pyb.ADCAll(12,0x70000)
    
    def getTemp(self):
        '''
        @brief Reads the temperature in celsius from the core.
        @details
        
        @return The core temperature in degrees Celsius
        '''
        return self.adc.read_core_temp()


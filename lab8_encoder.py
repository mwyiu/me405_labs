'''
@file       lab8_encoder.py

@brief      Lab 8 Encoder driver
@details    This file contains the EncoderDriver class that initializes an
            encoder object that reads the position of the motor. The user will
            be able to specify a reset value that the encoder will then read
            new position values from.
@author     Michael Yiu
@date       March 11, 2021
'''
import pyb

class EncoderDriver:
    '''
    @brief      Lab 8 Encoder
    @details    This class implements an optical encoder to measure the movement of a motor.
                To use this class, first initalize the motor object. Afterwards
                to initalize the encoder object pass in pins 1 and 2 and their 
                respective timer, timer channel, and period. Use the update() 
                function in a loop to constantly update the position of the 
                encoder, get_position() to return the position of the encoder 
                in degrees, and set_position() to set a new datum for the encoder.
    '''
    def __init__(self, pin_IN1, pin_IN2, timer, channel, period):
        '''
        @brief Creates an Encoder object and initializes the timer for the encoder.
        @param pin_IN1 A pyb.Pin object to use for encoder channel 1.
        @param pin_IN2 A pyb.Pin object to use for encoder channel 2.
        @param timer A pyb.Timer object to use for encoder reading.
        @param channel Channels for the encoder object.
        @param period Period of the timer channel.
        '''
        ## Copying the passed pin_IN1 object
        self.pin_IN1 = pyb.Pin(pin_IN1)
        ## Copying the passed pin_IN2 object
        self.pin_IN2 = pyb.Pin(pin_IN2)
        ## Copying the passed timer object
        self.timer = timer
        ## Copying the passed period
        self.period = period
        ## Initializing channel 1 to read encoder ticks
        self.ch1 = self.timer.channel(channel[0], pin=self.pin_IN1, mode=pyb.Timer.ENC_AB)
        ## Initializing channel 2 to read encoder ticks
        self.ch2 = self.timer.channel(channel[1], pin=self.pin_IN2, mode=pyb.Timer.ENC_AB)
        
        ## Initial tick position of the encoder
        self.prev_position = 0 
        ## Current reset position of the encoder
        self.p_reset = 0
        ## Current position of the encoder
        self.position = 0
        
    def update(self):
        '''
        @brief Updates the recorded position of the encoder.
        '''
        self.get_delta()
        self.position += self.p_delta

    def get_position(self):
        '''
        @brief Returns the most recently updated position of the encoder.
        @return The position of the encoder in degrees.
        '''
        return self.position*360/4000 #units of degrees
        
    def set_position(self, p_reset):
        '''
        @brief Resets the position to to a specified value.
        @param p_reset The specified reset position.
        '''
        self.position = p_reset
        
    def get_delta(self):
        '''
        @brief Returns the difference in recorded position between the two most recent calls to update()
        '''
        ## Position of the previous callout
        self.position1 = self.prev_position
        ## Position of the newest callout
        self.position2 = self.timer.counter()
        ## The delta position(ticks) between two callouts
        self.p_delta = self.position2 - self.position1
        self.prev_position = self.position2 # Updating the old position value to the new one
        
        if(abs(self.p_delta) >= (self.period+1)/2): # Fixing "bad" deltas
            if(self.p_delta > 0):
                self.p_delta -= (self.period+1)
            elif(self.p_delta < 0):
                self.p_delta += (self.period+1)

if __name__ == '__main__':
    ## Creating the IN1 pin for encoder 1
    pin_IN1 = pyb.Pin.cpu.B6
    ## Creating the IN2 pin for encoder 1
    pin_IN2 = pyb.Pin.cpu.B7
    ## Creating the IN1 pin for encoder 2
    pin_IN3 = pyb.Pin.cpu.C6
    ## Creating the IN2 pin for encoder 2
    pin_IN4 = pyb.Pin.cpu.C7
    ## Creating period for timer
    period = 0xFFFF
    ## Create the timer object used for PWM generation
    timer1 = pyb.Timer(4, prescaler = 0, period = period)
    ## Create the timer object used for PWM generation
    timer2 = pyb.Timer(8, prescaler = 0, period = period)
    ## Specifying the timer channels
    channel = (1, 2)
    ## Create an encoder 1 object and passing in the pins and timer
    encoder1 = EncoderDriver(pin_IN1, pin_IN2, timer1, channel, period)
    ## Create an encoder 2 object and passing in the pins and timer
    encoder2 = EncoderDriver(pin_IN3, pin_IN4, timer2, channel, period)
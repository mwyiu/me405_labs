'''
@file       lab3_main.py

@brief      Back-end for lab 3 that measures voltage change of the blue button.
@details    This file utilizes an analog-to-digital converter to measure the
            voltage change of the blue input button while it is being pressed.
            Each voltage measurement is in ADC counts and is accompanied by the
            time that it was recorded in microseconds. The voltage measurements
            are recorded in a temporary buffer array. This array, along with the
            time measurements, is sent serially to the PC front-end, where it 
            will be recorded, interpretted, plotted, saved to a csv file.
@author     Michael Yiu
@date       February 4, 2021
'''
from pyb import UART
import pyb
import array

## Initializes the UART connection
myuart = UART(2)

## State that tracks whether the user has pressed the blue button or not
button_pressed = None

## Value of the user command
val = None

def ButtonPress(which_pin):
    '''
    @brief      The function callback for the external interrupt  
    @details    This function is called when the external interrupt is activated.
                This function changes the state of the global state variable, 
                button_pressed, to True.
    @param  which_pin   Which pin is calling this interrupt
    '''
    global button_pressed 
    # Button has been pressed, change state to true
    button_pressed = True

## Initializing timer 2, with a frequency of 200 KHz
timer = pyb.Timer(2, freq=200000)

## Initializing the external interrupt on a falling edge of pin C13 
extint = pyb.ExtInt(pyb.Pin.cpu.C13, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback=ButtonPress)

## ADC Object using board pin PC0
adc = pyb.ADC(pyb.Pin.board.PC0)

## Creating a temporary buffer array to store the measured values from the ADC
buffer = array.array ('i', (0 for i in range (1001)))

while True:
    if(myuart.any() != 0):
        # Do this: check if any signals have been sent and record the signal
        val = myuart.readchar() #Recording the command the user has sent
    if(val == 71):
        # Do this: if the user has entered "G", the go command
        if(button_pressed == True and adc.read() > 5):
            # Do this: if the user has pressed the button and the voltage readout is greater than 5 ADC counts (to filter unwanted data)
            adc.read_timed(buffer, timer) #Record the ADC readout every 5 microseconds and record the data in the buffer
            myuart.write('button pressed\r\n') #Notify the user that they have pressed the button
            for i in range(1001):
                # Do this: send the measured data back to the PC in pairs of the format (microseconds, ADC count)
                myuart.write('{:},{:}\r\n'.format(5*i,buffer[i])) #Serially sending the data back to the PC. 5*i because data is measured in 5 microsecond intervals due to the 200 KHz timer frequency
                button_pressed = False #Resetting the button since data has finished collecting
                val = None #Clearing the command sent from the PC
'''
@file       lab9_main.py

@brief      Ball balancing platform implementation
@details    This file implements the touchpanel, motor, and encoder drivers to
            balance the ball on the platform. The touchpanel determines the
            current position of the ball on the platform and the motor is 
            actuated to correct its position. The encoder driver is used in
            conjunction with the motor driver to determine how much actuation
            is required by the motor.
@author     Michael Yiu & Alex Lewis
@date       March 17, 2021
'''
import pyb
from pyb import Pin
from Motor import MotorDriver
from encoder import EncoderDriver
from touchpanel import Scanner
import utime

class Controller:
    '''
    @brief      This class implements a controller object to control the ME 405 board.
    @details    This class combines the MotorDriver, EncoderDriver, and Scanner
                classes from motor.py, encoder.py, and touchpanel.py respectively.
                Altogether, this controller object aims to balance a rubber ball
                on the balancing platform by scanning and calculating the velocity
                of the ball and angular velocity of the platform. With this,
                a duty cycle is calculated and the motors are actuated to correct
                the position of the ball. To use this class, initalize the prior
                three objects (additional documentation is available on their
                class pages), and pass them into this object. Once initialized,
                in any order, employ the ball_update() and platform() functions
                to calculate the necessary velocities and then call duty_cycle()
                to calculate and send the appropriate PWM value to the motors.
    '''
    def __init__(self, K1, K2, K3, K4, motor1, motor2, touch_panel, encoder1, encoder2, nFault):
        '''
        @brief Creates a controller object to balance a ball on a platform.
        @param K1           Controller gain value for x_doubledot
        @param K2           Controller gain value for theta_y_doubledot
        @param K3           Controller gain value for x
        @param K4           Controller gain value for theta_y
        @param motor1       A motor object to control motor 1.
        @param motor2       A motor object to control motor 2.
        @param touch_panel  A touch panel object to measure position on the touch panel.
        @param encoder1     An encoder object to measure the position of motor 1.
        @param encoder2     An encoder object to measure the position of motor 2.
        @param nFault       Pin to detect for fault
        '''
        ## Copying the K-values into a tuple of size four
        self.K = [K1,K2,K3,K4] 
        ## Terminal resistance of the motor
        self.R = 2.21 #Ohms
        ## DC Voltage of the motor
        self.V_dc = 12 #Volts
        ## Torque constant
        self.K_t = 13.8/1000 #N-m/Amps
        ## Copying the passed motor 1 object
        self.motor1 = motor1
        ## Copying the passed motor 2 object
        self.motor2 = motor2
        ## Copying the passed touch panel object
        self.touch_panel = touch_panel
        ## Copying the passed encoder object for motor 1
        self.encoder1 = encoder1
        ## Copying the passed encoder object for motor 2
        self.encoder2 = encoder2
        ## Copyingthe nFault pin
        self.pin_nFault = nFault
        
        ## x-velocity of the ball
        self.x_d = 0
        ## x-position of the ball
        self.x = 0
        ## x-angle of the platform
        self.theta_x = 0
        ## x-anglular velocity of the platform
        self.theta_d_x = 0
        
        ## y-velocity of the ball
        self.y_d = 0
        ## y-position of the ball
        self.y = 0
        ## y-angle of the platform
        self.theta_y = 0
        ## y-anglular velocity of the platform
        self.theta_d_y = 0
        
        ## Time of last ball position reading
        self.ball_time = utime.ticks_ms()
        ## Time of last platform position reading
        self.platform_time = utime.ticks_ms()
        self.last_xposition = 0
        self.last_yposition = 0 
        self.last_x_theta = 0
        self.last_y_theta = 0
        
        ## Radius of the lever arm
        self.rm = 60 #mm
        ## Horizontal Distance from U-Joint to Push-Rod Pivot
        self.lp = 110 #mm
        
        ## Gear ratio between motor and belt powered arm. 
        self.gear_ratio = 4
    
        ## Initializing the external interrupt on a falling edge of pin B2
        self.extint = pyb.ExtInt(self.pin_nFault, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_UP, callback=self.nFault)
    
    def duty_cycle(self):
        '''
        @brief  This method calculates the duty cycle for the motors.
        @details The duty cycle is calculated depending on the current position
                 and velocity of the ball and angular position and velocty of the
                 platform. These values are multiplied by a proportional gain
                 K matrix to convert them into percent duty cycle.
        '''
        if self.touch_panel.position[2] == False:
            duty_cycle = 0
        else:
            ## Duty cycle is calculated for x and y and passed to the two motors. 
            duty_cycle = self.gear_ratio*(self.R/(self.V_dc*self.K_t))*-(self.K[0]*self.x_d + self.K[1]*self.theta_d_x + self.K[2]*self.x + self.K[3]*self.theta_x)
        self.motor1.set_duty(duty_cycle)
        
        if self.touch_panel.position[2] == False:
            duty_cycle = 0
        else:
            duty_cycle = self.gear_ratio*(self.R/(self.V_dc*self.K_t))*-(self.K[0]*self.y_d + self.K[1]*self.theta_d_y + self.K[2]*self.y + self.K[3]*self.theta_y)
            print(duty_cycle)
        self.motor2.set_duty(duty_cycle)
            
    def ball_update(self):
        '''
        @brief      This method calculates the velocity of the ball
        @details    The velocity of the ball is determined by comparing the 
                    difference in recorded position between the two most recent
                    calls of the touch panel's readall() function. The difference
                    is then divided by the time elapsed between calls. This is
                    done for both the x and y-directions.
        '''
        ##X and Y positions are updated
        self.touch_panel.readall()
        self.x = self.touch_panel.position[0]
        self.y = self.touch_panel.position[1]
        if self.touch_panel.position[2] == 1: 
            
            ## If there is contact, X and Y velocities are calculated
            time_delta = utime.ticks_diff(utime.ticks_ms(),self.ball_time)
            
            self.x_d = (self.touch_panel.position[0] - self.last_xposition[0])/(time_delta/1000) #mm/s
            self.y_d = (self.position[1] - self.last_yposition[1])/(time_delta/1000)  #mm/s
            
            
            ## Last x-position
            self.last_xposition = self.x
            ## Last y-position
            self.last_yposition = self.y
            self.ball_time = utime.ticks_ms()

            
    def platform(self):
        '''
        @brief      This method calculates the angular velocity of the ball.
        @details    The angular velocity of the platform is calculated by 
                    comparing the difference in recorded position between the 
                    two most recent of the encoder's update() function. The 
                    difference is then divided by the time elapsed between 
                    calls. This is done for both the x and y-directions.
        '''
        
        self.encoder1.update()
        self.encoder2.update()
        angle1 = self.encoder1.position*360/4000 #degs
        angle2 = self.encoder2.position*360/4000 #degs
        self.theta_x  = angle1*self.rm/self.lp
        self.theta_y  = angle2*self.rm/self.lp
        
        time_delta = utime.ticks_diff(utime.ticks_ms(),self.platform_time)
        
        self.theta_d_x = (self.theta_x - self.last_x_theta)/(time_delta/1000) #mm/s
        self.theta_d_y = (self.theta_y - self.last_y_theta)/(time_delta/1000) #mm/s
            
            
        ## Last x-angle
        self.last_x_theta = self.theta_x
        ## Last y-angle
        self.last_y_theta = self.theta_y
        self.platform_time = utime.ticks_ms()

    def nFault(self, which_pin):
        '''
        @brief This method turns off both motors when the external interrupt is triggered.
        @param  which_pin   Which pin is calling this interrupt
        '''
        motor1.nFault()
        motor2.nFault()
        print("Too much current! Motors turned off for safety.")


if 1:
    ## Width of the touch panel in millimeters
    width = 176
    ## Length of the touch panel in millimeters
    length = 100
    ## Center of the touch panel in millimeters
    center = (width/2, length/2)
    ## Pin object for the positive x pin
    x_p = Pin.cpu.A7
    ## Pin object for the negative x pin
    x_m = Pin.cpu.A1
    ## Pin object for the positive y pin
    y_p = Pin.cpu.A6
    ## Pin object for the negative y pin
    y_m = Pin.cpu.A0
    ## Initialzing a scanner object and passing in pin objects
    panel = Scanner(x_p, x_m, y_p, y_m, width, length, center)

    ## Creating the nSLEEP pin for the motor driver
    pin_nSLEEP = pyb.Pin.cpu.A15
    ## Creating the IN1 pin for the motor driver
    motor_IN1 = pyb.Pin.cpu.B4
    ## Creating the IN2 pin for the motor driver
    motor_IN2 = pyb.Pin.cpu.B5
    ## Creating the IN1 pin for the motor driver
    motor_IN3 = pyb.Pin.cpu.B0
    ## Creating the IN2 pin for the motor driver
    motor_IN4 = pyb.Pin.cpu.B1
    ## Create the timer object used for PWM generation
    motor_timer = pyb.Timer(3, freq=20000)
    ## Specifying the timer channels for motor 1
    motor1_channel = (1, 2)
    ## Specifying the timer channels for motor 2
    motor2_channel = (3, 4)
    ## Create a motor object 1 and passing in the pins and timer
    motor1 = MotorDriver(pin_nSLEEP, motor_IN2, motor_IN1, motor_timer, motor1_channel)
    ## Create a motor object 2 and passing in the pins and timer
    motor2 = MotorDriver(pin_nSLEEP, motor_IN4, motor_IN3, motor_timer, motor2_channel)    
    
    ## Creating the IN1 pin for encoder 1
    encoder_IN1 = pyb.Pin.cpu.B6
    ## Creating the IN2 pin for encoder 1
    encoder_IN2 = pyb.Pin.cpu.B7
    ## Creating the IN1 pin for encoder 2
    encoder_IN3 = pyb.Pin.cpu.C6
    ## Creating the IN2 pin for encoder 2
    encoder_IN4 = pyb.Pin.cpu.C7
    ## Encoder 1 timer channel
    encoder1_timer = 4
    ## Encoder2 timer channel
    encoder2_timer = 8
    ## Create an encoder 1 object and passing in the pins and timer
    encoder1 = EncoderDriver(encoder_IN1, encoder_IN2, encoder1_timer)
    ## Create an encoder 2 object and passing in the pins and timer
    encoder2 = EncoderDriver(encoder_IN3, encoder_IN4, encoder2_timer)
    
    ## Creating the nFault pin for the motor driver, uses CPU Pin B2
    pin_nFault = pyb.Pin.cpu.C13
    
    ## Controller gain value for x_doubledot
    K1 = .05
    ## Controller gain value for theta_y_doubledot
    K2 = .05
    ## Controller gain value for x
    K3 = .05
    ## Controller gain value for theta_y
    K4 = .05
    ## Create a controller object and passing in motor, encoder, touch panel objects, and controller gains
    control = Controller(K1,K2,K3,K4,motor1,motor2,panel,encoder1,encoder2,pin_nFault)
    control.motor1.enable() # turning on motor 1
    control.motor2.enable() # turning on motor 2
    while True:
        control.ball_update()
        control.platform()
        control.duty_cycle()
        utime.sleep(.15)
'''
@file mainpage.py

@mainpage

@section page_intro Introduction

This portfolio is a culmination of all the documentation written for ME 405:
Mechatronics taken during Winter 2021. During this term, two homeworks assignments
were completed as well as nine labs. Please use the navigation bar on the left
to go to each individual assignment page. Each page contains a synopsis of the
lab in the form of an overview, links to the source code, required files for the
assignment, and documentation for each class, function, and variable created. 
I hope you enjoy reading my documentation as much as I enjoyed this class.

@page page_hw0 Homework 0: State Transition Diagrams
@date January 10, 2021
@author Michael Yiu
@copyright Michael Yiu

@section sec_hw0code State Transition Diagrams Source Code
No coding was required for this assignment.

@section hw0_intro Overview
The program created in this lab serves as a review to Python for ME 405 students. 
It's main purpose is to refamiliarize students with doxygen and code documentation.
In this homework assignment, a function was created that returned the change 
between a prince in integers of pennies and a payment in the form of a size 8 
tuple (pennies, nickels, dimes, quarters, ones, fives, tens, twenties).

@section sec_hw0statediagram State Diagram
Below is a state diagram of the vending machine.
@image html hw0statediagram.jpg "Figure 1: Vending Machine State Diagram" width=75%

@page page_hw1 Homework 1: Python Review
@date January 13, 2021
@author Michael Yiu
@copyright Michael Yiu

@section sec_hw1code Python Review Source Code
The source code can be found at:

https://bitbucket.org/mwyiu/me405_labs/src/master/hw1.py

@section hw1_intro Overview
The program created in this lab serves as a review to Python for ME 405 students. 
It's main purpose is to refamiliarize students with doxygen and code documentation.
In this homework assignment, a function was created that returned the change 
between a prince in integers of pennies and a payment in the form of a size 8 
tuple (pennies, nickels, dimes, quarters, ones, fives, tens, twenties).

@section hw1_file File: hw1.py
This file inclues the function getChange(). This function calculates the change 
between a price (a value in integers of pennies USD) and a payment (a tuple of 
size 8 where each position represents an integer value of pennies, nickels, dimes, 
quarters, one dollar bills, five dollar bills, ten dollar bills, twenty dollar bills, 
respectively). The change is returned back as a tuple in the same format of payment, 
however in the fewest denominations as possible.

@page page_lab1 Lab 1: Reintroduction to Python and Finite State Machines
@date January 21, 2021
@author Michael Yiu
@copyright Michael Yiu

@section sec_lab1code Reintroduction Source Code
The source code can be found at:

https://bitbucket.org/mwyiu/me405_labs/src/master/lab1.py

@section lab1_intro Overview
This lab serves as a reintroduction and refresher to Python for ME 405 students.
This program simulates a vending machine using a finite state machine. The 
vending machine displays the current balance and updates the balance each time
money is entered. Acceptable currency includes pennies, nickels, dimes, quarters, 
ones, fives, tens, and twenties. At any point, the user can select a beverage, 
and if the current balance is greater than the beverage price, the beverage and 
the appropriate amount of change will be dispensed. Alternatively, the user can 
change their mind at any point and press the eject button for the machine to 
dispense the current balance.  

@section sec_statediagram State Diagram
Homework 0 was done as a precursor to this lab assignment. In Homework 0, we 
created a finite-state machine diagram of the vending machine. An image of the
state diagram can be seen at \ref sec_hw0statediagram. This state diagram was 
used as a reference for the functions created in Lab 1.

@section lab1_file File: lab1.py
This file contains the functions Change() and printWelcome(). Change() 
calculates the change between the current balance and the drink price and returns
the change in the fewest denominations as possible. If balance is less than the
drink price, the current balance will be returned. printWelcome() prints the 
welcome message displayed on the vending machine on startup.

@page page_lab2 Lab 2: Think Fast!
@date January 28, 2021
@author Michael Yiu
@copyright Michael Yiu

@section sec_lab2code Think Fast! Source Code
The source code can be found at:

https://bitbucket.org/mwyiu/me405_labs/src/master/lab2.py

@section lab2_intro Overview
This lab serves as a refresher to the Nucleo-L476RG for ME 405 students. 
Additionally, this lab serves as an introduction to the external input command.
In this lab, the student creates a program/game that measures a person's reaction 
time responding to a light and pressing a button. An LED will turn on randomly 
every two or three seconds and stays on for one second. The player will attempt
to press the button as soon they see the light. Pressing the button will trigger
and external interrupt that will turn off the light and calculate the time it 
took for the player to react. This will continue in a loop forever or until the 
player decides to quit, at which point the average reaction time will be 
calculated.

@section lab2_file File: lab2.py
This file contains the function ButtonPress(). ButtonPress() tracks whether or 
not the blue input button, on pin C13, has been pressed. Additionally, it records
the time that it was pressed using timer channel 2. The purpose for recording 
this time is to determine the reaction time of the player hitting the input 
button as soon as the LED, on pin A5, is turned on.

@page page_lab3 Lab 3, Pushing the Right Buttons
@date February 4, 2021
@author Michael Yiu
@copyright Michael Yiu

@section sec_lab3code Pushing the Right Buttons Source Code
The source code can be found at:

https://bitbucket.org/mwyiu/me405_labs/src/master/lab3_main.py

https://bitbucket.org/mwyiu/me405_labs/src/master/lab3_UI.py

https://bitbucket.org/mwyiu/me405_labs/src/master/data.csv

@section lab3_intro Overview
This lab serves as an introduction to serial communication between the PC and 
the Nucleo-L476RG. The student will create a simple user interface that 
facilitates communication between the user, PC, and Nucleo. In this lab, the 
student will measure the voltage change on the Nucleo of the blue input button 
as it is being pressed using an analog-to-digital converter. The voltage will be
measured in ADC counts for 5 milliseconds at a 5 microsecond interval. Once all
the data has been collected, the Nucleo will serially send the measurements to
the PC front end, where it will be recorded, plotted, and saved onto a csv file.

@section sec_adcplot Voltage versus Time Plot
Below is a plot of the button voltage versus time.
@image html lab3adcplot.jpg "Figure 1: Voltage of the Button Versus Time" width=50%

@section lab3_file1 File: lab3_UI.py
This file acts as the front-end user interface for lab 3 by establishing serial
communication between the PC and the Nucleo. It allows the user to send a "go"
command to the Nucleo, which prompts it to wait for the user to press the blue
input button. In the back-end, lab3_main.py, the Nucleo measures the voltage 
change of the button and serially sends it back to the front-end in pairs with 
format (time, voltage reading). lab3_UI.py then records the data, interprets it,
plots it, and writes the data onto a csv file. 

@section lab3_file2 File: lab3_main.py
This file acts the back-end for lab 3. It contains the function ButtonPress(),
which tracks whether or not the blue input button, on pin C13, has been pressed.
The purpose of this file is to measure the voltage change using an analog-to-digital
converter. Every 5 microseconds, a measurement in ADC counts will be recorded,
up until 5 milliseconds has elapsed. Once the voltage change has been measured, 
the measurements will be sent serially to the front-end, lab3_UI.py, to be 
recorded, plotted, and saved onto a csv file.

@page page_lab4 Lab 4, Hot or Not?
@date February 11, 2021
@author Collaboration between Michael Yiu & Ben Bons

@section sec_lab4code Hot or Not? Source Code
The source code can be found at:

https://bitbucket.org/bbons/me-405-lab-4/src/master/lab4_main.py

https://bitbucket.org/bbons/me-405-lab-4/src/master/mcp9808.py

https://bitbucket.org/bbons/me-405-lab-4/src/master/stm32coreTemp.py

@section lab4_intro Overview
This lab serves as an introduction to Inter-Integrated Circuit, I2C, and sensors.
Students will work together in teams of two to cooperatively write code, setup
their hardware, and use it to take temperature measurements. They will connect
a MCP9808 temperature sensor to the Nucleo through I2C. Temperature measurements
read by the sensor will be sent back to the Nucleo to be saved onto a csv file,
along with the Nucleo's internal temperature.

@section sec_tempplot Temperature versus Time Plot
Below is a plot of the temperature versus time. These measurements were recorded
between 2/10 5:30PM and 2/11 1:30AM; they were measured underneath a laptop
stand and near a windowsill.
@image html lab4tempplot.jpg "Figure 1: Temperature versus Time" width=50%

@section lab4_file1 File: stm32coreTemp.py
This file serves as the Nucleo's internal temperature reader. The file contains
the class coreTemp that measures the internal temperature using an analog-to-digital
converter.

@section lab4_file2 File: mcp9808.py
This file serves as the external MCP9808 temperature reader. It communicates with
the Nucleo using I2C. It can measure the temperature in either Celsius or 
Fahrenheit.  

@section lab4_file3 File: lab4_main.py
This file serves as the data collection tool. It creates two objects, coreTemp 
and sensor, which is connected to the external temperature reader. The code
measures the internal temperature as well as the external temperature every
minute for eight hours. Once data collection has finished, or the user enters
Ctrl-C to exit, the Nucleo will generate a csv file with all of the data. From 
the data, the student is expected to produce a plot of temperature vs. time and
"to provide a short one-sentence explanation of when and where the measurements
were made."

@page page_lab5 Lab 5: Feeling Tipsy?
@date February 18, 2021
@author Michael Yiu
@copyright Michael Yiu

@section lab5_intro Overview
This lab assignment is the first preparation step for the upcoming term project.
In this lab, the student will design a controller to balance a ball on top of
a pivoting platform. Hand calculations will be done to analyze the motion
of the system and to develop a model that represents it. 

@section lab5_schematic Schematic and Parameters
@image html lab5parameters.PNG "Figure 1: Parameters" width=50%

@section lab5_assumptions Assumptions Made:
    1. The two planes of motion for the platform are uncoupled.
    2. Small angle approximation.
    3. Masses and moments of inertias of the lever arm and push-rod are negligible.
    4. A no-slip condition between the ball and the platform.

@section lab5_handcalcs Hand Calculations
I first started by drawing the 4-bar linkage OADQ, and using it to develop a
kinematic relationship between the motion of the motor and the position of the
platform. Since the forces at D and Q are equal and opposite, I set them equal
to each other to find the velocity and acceleration relationship between them.
    Afterwards, I took the sum of moments at different locations to eliminate the
unknown reaction force at Q. The first sum of moments was at point A of the
lever arm and push-rod assembly.
@image html lab5handcalc1.jpg "Figure 2: Hand Calculations 1" width=50%

Next, I took the sum of moments at point O of the ball and platform assembly.
In order to eliminate the acceleration variables of the ball, I developed a 
kinematic relationship between the motion of the ball and the motion of the 
platform. A no-slip condition was assumed.
@image html lab5handcalc2.jpg "Figure 3: Hand Calculations 2" width=50%

Lastly, I took the sum of moments of the ball and platform at point O.
Using all the equations I have developed thus far, I combined them together
and wrote them into a matrix format with coefficients of x-double-dot and 
theta_y-double-dot, respectively the acceleration of the ball parallel 
to the plate and the angular acceleration of the platform.
@image html lab5handcalc3.jpg "Figure 4: Hand Calculations 3" width=50%

@page page_lab6 Lab 6: Simulation or Reality?
@date February 25, 2021
@author Michael Yiu
@copyright Michael Yiu

@section sec_lab6code Simulation or Reality? Source Code
The source code can be found at:

https://bitbucket.org/mwyiu/me405_labs/src/master/Lab6.m

https://bitbucket.org/mwyiu/me405_labs/src/master/Lab6_models.slx

@section lab6_intro Overview
This lab assignment is the second preparation step for the upcoming term project.
Building from Lab 5, the student will develop a simulation on either Python
or Matlab/Simulink that will model a simplified version of the platform. The
student will further linearize the model from Lab 5 using the Jacobian method,
and will plot the simulations for a variety of scenarios.

@section lab6_openloop Open-Loop Model
@image html lab6openloop.PNG "Figure 1: Open-loop model of the system" width=75% 

@section lab6_closedloop Closed-Loop Model
@image html lab6closedloop.PNG "Figure 2: Closed-loop model of the system" width=75% 
        
@section lab6_openloopcases Open-Loop Model Cases
Case A, the ball is initially at rest on the platform, and the platform is level
with no torque applied by the motor. This case was simulated for 1 second. Seen 
below in Figure 3, both velocities, positions, and angles are zero. We expect
this to happen as the ball is at rest, over the center of gravity of the platform,
and no external forces are being applied.
@image html lab6caseA.jpg "Figure 3: Case A Response" width=75%

Case B, the ball is initially at rest on the platform, though located 5 cm 
horizontally away from the center of gravity. No torque is applied by the motor.
This case was simulated for 0.4 seconds. Seen below in Figure 4, as time progresses
all values increase, curving upwards. We expect this to happen since the ball
is no longer over the center of gravity of the platform. The ball's weight at 
its new position causing a torque on the platform, which changes the angle of
the platform and causes the ball to roll. 
@image html lab6caseB.jpg "Figure 4: Case B Response" width=75%

Case C, the ball is initially at rest on the platform with no torque applied by
the motor. However, the platform is no longer level, as it is inclined 5 degrees
directly above its center of gravity. This case was simulated for 0.4 seconds.
Seen below in Figure 5, the plots are very similar to the plots of Figure 4. We
expect this to occur since the platform is no longer level, which will cause
the ball to roll from its initial position. 
@image html lab6caseC.jpg "Figure 5: Case C Response" width=75%

Case D, the ball is initially at rest on the platform directly above its center
of gravity. The platform is level and an impulse of 1 mN-m-s is applied by the
motor. This case was simulated for 0.4 seconds. The values of the plot
initially start at zero but gradually become negative as time progresses. We 
expect this because a positive torque from the motor results in a negative
rotation of the platform. Thus, the ball will roll down in the negative direction.
@image html lab6caseD.jpg "Figure 6: Case D Response" width=50%

@section lab6_closedloopcases Closed-Loop Model Cases
This simulation uses the same initial conditions as Case B in the open-loop model.
Closing the loop and adding a controller introduces negative feedback that 
causes the system to eventually reach steady-state. The controller values used 
are K = [-0.05 -0.02 -0.3 -0.2]. Having a controller is necessary, as it will
eventually stabilize the platform by causing the velocities to decay to near 
zero.
@image html lab6feedback1.jpg "Figure 7: Velocity and Position Response" width=75%
@image html lab6feedback2.jpg "Figure 8: Angular Velocity and Angle Response" width=75%

@page page_lab7 Lab 7: Feeling Touchy
@date March 4, 2021
@author Michael Yiu
@copyright Michael Yiu

@section sec_lab7code Feeling Touchy Source Code
The source code can be found at:

https://bitbucket.org/mwyiu/me405_labs/src/master/lab7_touchpanel.py

@section lab7_intro Overview
This lab assignment is the third preparation step for the term project. This
week, the student will be assembling and soldering hardware, which consists 
of a resistive touch panel and FPC (flexible printed circuitry) adapter. Once 
all the hardware has been connected, the student will develop a hardware driver
to interface with the touch panel. This driver will utilize an ADC counter to
read the X-position and Y-position of the point of contact. A Z-position will
be read as well; this reading will return a boolean value that represents 
whether or not there is contact with the touch panel.

@section lab7_file1 File: lab7_touchpanel.py
This file contains the class scanner that scans the position of contact on the
touch panel. It determines if contact has been made by measuring a voltage in
the z-direction. For position, it measures a voltage in the x and y-directions.
Although the touch panel is very responsive, it takes about four microseconds for
the measured signal to settle. The scanner class deals with this by waiting 
four microseconds before taking a measurement reading. The scanner has four
primary functions: scanning the x-position, scanning the y-position, scanning
the z-position, and scanning all three positions at once. Altogether, the
scanner is able to measure all three positions in under 1,500 microseconds,
which is a crucial procedure for Lab 8. 

@section lab7_setup Hardware Setup
Figure 1 is an image of my hardware setup for lab 7. The touch panel is 
connected to a FPC adapter that is connected with a four wire cable to the X6 
pin located on the PCB panel. Light soldering was required to join the FPC 
adapter and pin headers. In this specific instance, the touch panel pins, x_p, 
x_m, y_p, and y_m are respectively connected to X6 pins 4, 2, 1, and 3. 
@image html lab7setup.jpg "Figure 1: Hardware Setup" width=50%

Figure 2 shows a close up of the soldered FPC adapter. The second pin from the
right is especially thick because during my first pass through, the solder did
not bridge the connection between the pin and FPC adapter.
@image html lab7closeup.jpg "Figure 2: FPC Close up" width=50%

@section lab7_testing Testing
@image html lab7testing.PNG "Figure 3: Runtime Benchmark" width=50%
To guarantee that this touch panel works cooperatively for the term project, it
must scan the x, y, and z positions in 1,500 microseconds or less. A runtime
benchmark was conducted on my code to ensure that it completes in 1,500 
microseconds or less. As seen in Figure 3, my scanner object is able to read 
all of the directions in approximately 1,042 microseconds. In that time, my 
scanner object defines the appropriate pin objects and measures the
corresponding voltage and position. The order in which they are scanned are x, 
y, and then z. This was done 100 times and averaged to gain a better estimate
of the total runtime.

Through testing, I discovered that a conversion factor was necessary to convert
the x-ADC counts and y-ADC counts. I found that the minimum and maximum ADC
counts in the x-direction are 200 and 3,800 and 375 and 3,575 for the 
y-direction. I calculated the slope and intercept value using a system of 
equations, with one equation at each edge. The exact m and b values for the 
y= m*x + b equation can be found in the source code.

@page page_lab8 Lab 8: Term Project Part I
@date March 11, 2021
@author Michael Yiu

Although we were asked to organize in teams of two, I was unable to find a partner
in time and had to complete lab 8 by myself.
@copyright Michael Yiu

@section sec_lab8code Term Project Part I Source Code
The source code can be found at:

https://bitbucket.org/mwyiu/me405_labs/src/master/lab8_motor.py

https://bitbucket.org/mwyiu/me405_labs/src/master/lab8_encoder.py

@section lab8_intro Overview
This lab assignment is the first of two for the term project. In this week, the
student will create drivers for the motor and encoder. The MotorDriver class 
allows the user to initialize a motor object by passing in the appropriate pins,
timer, and timer channels. The EncoderDriver class initializes an encoder object
in a similar fashion. With these two classes, the student can begin to incorporate
the closed-loop system developed in Lab 6.

@section lab8_file1 File: lab8_motor.py
This file contains the MoterDriver class that initializes a motor object. The
motor is driven using PWM output and includes an external interrupt stops all the
motors if a fault has been detected in Pin B2. Pin B2 is active low and is 
generally activated by an over-current condition. This class includes three 
main functions: enable() , turning on the motors, disable() , turning off the
motors, and set_duty() , setting the duty cycle of the motor to a specific value.

@section lab8_file2 File: lab8_encoder.py
This file contains the EncoderDriver class that initializes an encoder object.
The encoder used in this lab has a CPR value of 1000, with 4 pulses per cycle.
Therefore, it has a PPR value of 4000 ticks per revolution. This class includes 
four main functions: update(), which updates the position of the encoder (in units
of degrees), get_position(), which returns the current value of the encoder, 
set_position(), which resets the position to a user specified value, and 
get_delta(), which calculates the difference between the two latest position 
calls. Lastly, the get_delta() function is able to identify "bad" deltas, those
that either overflow or underflow, and "fixes" them by either adding or 
subtracting half the period plus one. 

@page page_lab9 Lab 9: Term Project Part II
@date March 17, 2021
@author Collaboration between Michael Yiu & Alex Lewis

@section sec_lab9code Term Project Part II Source Code
The source code can be found at:

https://bitbucket.org/mwyiu/term-project/src/master/lab9_main.py

https://bitbucket.org/mwyiu/term-project/src/master/motor.py

https://bitbucket.org/mwyiu/term-project/src/master/encoder.py

https://bitbucket.org/mwyiu/term-project/src/master/touchpanel.py

@section lab9_intro Overview
This lab is the culmination of the last four labs. In this lab, we will
integrate the hardware, motor and encoder drivers, and system model altogether.
The ideal goal is to have the rubber ball balanced on the platform but trial and
error will have to be conducted to determine the best controller gain values. The
video seen below displays the performance of the final system.

@section lab9_video Performance Video
\htmlonly
<iframe width="560" height="315" src="https://www.youtube.com/embed/YlG8B2QPm_M" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
\endhtmlonly

As seen in this YouTube video, Alex and I had limited success on our ball
balancing platform. Shown is Alex's platform shifting the ball back and forth 
towards the center but it is eventually thrown off. We'd like to note that the 
x-axis motor was not functioning properly due to a loose set screw on the gear
connected to the motor shaft.

@section lab9_file1 File: lab9_main.py
lab9_main.py contains the controller class and code to initialize and run the 
controller. The controller class houses four functions: an initializer, 
duty_cycle(), ball_update(), and platform(). duty_cycle() uses the four state 
variables to calculate the duty cycle to be passed to the motors. It sets the 
duty cycle for both motors each time it is called. ball_update() tracks the 
position and velocity of the ball using the touch sensor every time it is called.
platform() tracks the angle an angular velocity of the platform using information 
from the encoders on each motor. lab9_main.py allows for different gain values 
to be tested on the controller. 

@section lab9_file2 File: motor.py
This file is a slightly modified version of the motor file created in Lab 8. The
external interrupt was removed and was instead placed in the lab9_main.py file.
This was due to the main file having the Controller class that controlled both
motors, so it became much easier to have it control the nFault pins rather than
the motors. The nFault pin is still on Pin B2, which is active low; Pin B2 is
typically triggered by an over-current solution. This class includes three 
main functions: enable(), turning on the motors, disable(), turning off the
motors, and set_duty(), setting the duty cycle of the motor to a specific value.

@section lab9_file3 File: encoder.py
encoder.py contains the EncoderDriver class. This class allows the user to track
the position of the motor. This class contains four methods: an initializer, 
update(), get_delta(), and set_position(). The update() method calculates a new
delta value from the encoder it is tracking and adds it to a running position 
tally. This occurs every time the update() method is called. get_delta() returns
the current delta value. set_position() allows the user to set the current encoder
position to any value. 

@section lab9_file4 File: touchpanel.py
This file is a slightly modified version of the touchpanel file used in Lab 6.
However, for all intents and purposes, it's essentially the same file. The 
Scanner class scans the position of contact on the touch panel as an x, y, and 
z position. There is a four microsecond delay to wait for the settling time and
all three positions are measured in under 1,500 microseconds. These position
values are stored in a postion variable in the format of a tuple, rather than
returned, to improve optimization.
'''
'''
@file       hw1.py
@brief      Homework 1, Python Review
@details    This file contains only the function getChange(). More information
            on the function can be seen below.
@author     Michael Yiu
@date       January 13, 2021
'''
# payment = (3, 0, 0, 2, 1, 0, 0, 1)
def getChange(price, payment):
    '''
    @brief      Calculates the change between a price and the payment entered.
    @details    This function receives a price and payment input. The price is 
                an integer value in cents, and the payment is a tuple of size 8
                that contains the number of denominations (pennies, nickles, 
                dimes, quarters, ones, fives, tens, and twenties) used. The change
                is calculated and returned in the fewest denominations as possible.
    @param      price       An integer representing the price of the item in cents.
    @param      payment     A tuple of size 8 representing the price of the 
                            item with an integer number of pennies, nickles, 
                            dimes, quarters, ones, fives, tens, and twenties.
    @return     An error message if an invalid input has been entered or the 
                change in the format of \p payment.
    '''
    error = "Please enter a valid input"
    try:
        money = (1*payment[0] + 5*payment[1] + 10*payment[2] + 25*payment[3] + 
                 100*payment[4] + 500*payment[5] + 1000*payment[6] + 
                 2000*payment[7])
    except:
        return error
    if(isinstance(price, int) != True or price < 0):
        return error
    change = money - price
    if(change < 0):
        error = "You still need to pay ${:.2f}.".format(abs(change)/100)
        return error
    else:
        twenties = int(change // 2000)
        change %= 2000
        tens = int(change // 1000)
        change %= 1000
        fives = int(change // 500)
        change %= 500
        ones = int(change // 100)
        change %= 100
        quarters = int(change // 25)
        change %= 25
        dimes = int(change // 10)
        change %= 10
        nickels = int(change // 5)
        change %= 5
        pennies = int(change // 1)
        change %= 1
        bills = (pennies, nickels, dimes, quarters, ones, fives, tens, twenties)
        return bills
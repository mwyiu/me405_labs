'''
@file       lab1.py

@brief      Lab 1, Reintroduction to Python and Finite State Machines
@details    This lab simulates a vending machine. The user can enter money at
            any time by entered the 0-7 keys. The user can select drinks by
            entering c, d, p, or s. Alternatively, if the user changes their
            mind, they can enter e to eject the money entered.
@author     Michael Yiu
@date       January 21, 2021
'''

import keyboard

## Intial state of the vending machine
state = 0
## State that tracks whether a key has been pressed or not
was_pressed = False

def Change():
    '''
    @brief      Computes change of the current balance and the price of the drink.
    @details    Returns the change in a tuple of size 8, with the lowest 
                quantity of denominations.
    @return     The change in a size 8 tuple: (pennies, nickels, dimes, quarters, ones, fives, tens, twenties)
    '''
    change = balance - drink_price
    if(change < 0):
        change = balance
    twenties = change // 2000 # Determine number of twenties
    change %= 2000 # Subtract number of twenties from change
    tens = change // 1000 # Determine number of tens
    change %= 1000 # Subtract number of tens from change
    fives = change // 500 # Determine number of fives
    change %= 500 # Subtract number of fives from change
    ones = change // 100 # Determine number of ones
    change %= 100 # Subtract number of dollars from change
    quarters = change // 25 # Determine number of quarters
    change %= 25 # Subtract number of quarters from change
    dimes = change // 10 # Determine number of dimes
    change %= 10 # Subtract number of dimes from change
    nickels = change // 5 # Determine number of nickels
    change %= 5 # Subtract number of nickels from change
    pennies = change // 1 # Determine number of pennies
    change %= 1 # Subtract number of pennies from change
    change = (pennies, nickels, dimes, quarters, ones, fives, tens, twenties) # Write number of denominations into a tuple
    return change 

def printWelcome():
    '''
    @brief Prints a welcome message.
    @details    A welcome message is printed to the user with the beverage 
                prices on the Vendotron machine.
    '''
    print("Welcome to Vendotron. Please enter money and select a beverage.\n"
           "   c for Cuke: $0.75\n"
           "   d for Dr.Pupper: $0.99\n"
           "   p for Popsi: $1.50\n"
           "   s for Spryte: $2.00\n")

while True:
    if state == 0:
    # Do this: if initializing
        '''
        Perform state 0 operations
        this init state, initializes the FSM itself, with all the
        code features already set up
        '''
        ## The current balance of money in the vending machine
        balance = 0
        ## The price of the drink selected
        drink_price = 0
        ## The name of the drink selected
        drink_name = None
        printWelcome() # print welcome message
        state = 1 # change state from 0 to 1
            
    elif state == 1:
        '''
        Waits for the user to either enter money, select a drink, or to hit the
        eject button.
        '''
        if(keyboard.is_pressed('0')):
            # Do this: if 0 was pressed
            if not was_pressed:
                ## A temp variable to track the key pressed
                key_pressed = "0"
                state = 2 # change to state 2, balance update
                was_pressed = True #change pressed state to True
        elif(keyboard.is_pressed('1')):
            # Do this: if 1 was pressed
            if not was_pressed:
                key_pressed = "1"
                state = 2 # change to state 2, balance update
                was_pressed = True #change pressed state to True
        elif(keyboard.is_pressed('2')):
            # Do this: if 2 was pressed
            if not was_pressed:
                key_pressed = "2"
                state = 2 # change to state 2, balance update
                was_pressed = True #change pressed state to True
        elif(keyboard.is_pressed('3')):
            # Do this: if 3 was pressed
            if not was_pressed:
                key_pressed = "3"
                state = 2 # change to state 2, balance update
                was_pressed = True #change pressed state to True
        elif(keyboard.is_pressed('4')):
            # Do this: if 4 was pressed
            if not was_pressed:
                key_pressed = "4"
                state = 2 # change to state 2, balance update
                was_pressed = True #change pressed state to True
        elif(keyboard.is_pressed('5')):
            # Do this: if 5 was pressed
            if not was_pressed:
                key_pressed = "5"
                state = 2 # change to state 2, balance update
                was_pressed = True #change pressed state to True
        elif(keyboard.is_pressed('6')):
            # Do this: if 6 was pressed
            if not was_pressed:
                key_pressed = "6"
                state = 2 # change to state 2, balance update
                was_pressed = True #change pressed state to True
        elif(keyboard.is_pressed('7')):
            # Do this: if 7 was pressed
            if not was_pressed:
                key_pressed = "7"
                state = 2 # change to state 2, balance update
                was_pressed = True #change pressed state to True
        elif(keyboard.is_pressed('c')):
            # Do this: if c was pressed
            if not was_pressed:
                key_pressed = "c"
                state = 3 #move to state 3, drink selection
                was_pressed = True #change pressed state to True
        elif(keyboard.is_pressed('d')):
            # Do this: if d was pressed
            if not was_pressed:
                key_pressed = "d"
                state = 3 #move to state 3, drink selection
                was_pressed = True #change pressed state to True
        elif(keyboard.is_pressed('p')):
            # Do this: if p was pressed
            if not was_pressed:
                key_pressed = "p"
                state = 3 #move to state 3, drink selection
                was_pressed = True #change pressed state to True
        elif(keyboard.is_pressed('s')):
            # Do this: if s was pressed
            if not was_pressed:
                key_pressed = "s"
                state = 3 #move to state 3, drink selection
                was_pressed = True #change pressed state to True
        elif(keyboard.is_pressed('e')):
            # Do this: if e was pressed
            if not was_pressed:
                state = 4 #move to state 4, dispencing change and/or drink
                was_pressed = True #change pressed state to True
        else:
            was_pressed = False
    
    elif state == 2:
        '''
        Adds the value of the currency added to the balance.
        '''
        if key_pressed == "0":
            balance += 1
            print("A penny has been entered. Current balance: ${:.2f}".format(balance/100))
        elif key_pressed == "1":
            balance += 5
            print("A nickel has been entered. Current balance: ${:.2f}".format(balance/100))
        elif key_pressed == "2": 
            balance += 10
            print("A dime has been entered. Current balance: ${:.2f}".format(balance/100))
        elif key_pressed == "3": 
            balance += 25
            print("A quarter has been entered. Current balance: ${:.2f}".format(balance/100))
        elif key_pressed == "4": 
            balance += 100
            print("A dollar bill has been entered. Current balance: ${:.2f}".format(balance/100))
        elif key_pressed == "5": 
            balance += 500
            print("A five dollar bill has been entered. Current balance: ${:.2f}".format(balance/100))
        elif key_pressed == "6": 
            balance += 1000
            print("A ten dollar bill has been entered. Current balance: ${:.2f}".format(balance/100))
        elif key_pressed == "7": 
            balance += 2000
            print("A twenty dollar bill has been entered. Current balance: ${:.2f}".format(balance/100))
        if(balance >= drink_price):
            state = 4 #move to state 4, dispencing change and/or drink
        else:
            state = 1 #move back to state 1, checking for inputs
    
    elif state == 3:
        '''
        Selects a drink and either moves to dispensing the drink if the current
        balance is greater than the drink price, or displays a message
        indiciating that more funds are required.
        '''
        if key_pressed == "c":
            drink_price = 75
            drink_name = "Cuke"
            print("\nCuke was selected.")
        elif key_pressed == "d":
            drink_price = 99
            drink_name = "Dr. Pupper"
            print("\nDr. Pupper was selected.")
        elif key_pressed == "p":
            drink_price = 150
            drink_name = "Popsi"
            print("\nPopsi was selected.")
        elif key_pressed == "s":
            drink_price = 200
            drink_name = "Spryte"
            print("\nSpryte was selected.")
        if balance >= drink_price:
            state = 4 #move to state 4, dispencing change and/or drink
        else:
            print("Not enough money entered, still need ${:.2f}.\n".format((drink_price-balance)/100))
            state = 1 #move back to state 1, checking for inputs
    elif state == 4:
        '''
        Dispenses drink and change if balance is greater than drink price. 
        Otherwise ejects the current balance. In both cases, the vending machine
        is sent back to its initialization state.
        '''
        ## The difference between the current balance and price of the drink selected
        change = Change()
        if(drink_name and balance >= drink_price):
            # Do this: if drink was selected and money entered is greater than drink price
            print(drink_name + " dispensed.\n"
                  "Change ejected: ${:.2f}\n"
                  "   {:d} pennies\n"
                  "   {:d} nickels\n"
                  "   {:d} dimes\n"
                  "   {:d} quarters\n"
                  "   {:d} dollars\n"
                  "   {:d} fives\n"
                  "   {:d} tens\n"
                  "   {:d} twenties\n".format((balance-drink_price)/100,change[0],change[1],change[2],change[3],change[4],change[5],change[6],change[7]))
        else:
            print("Change ejected: ${:.2f}\n"
                  "   {:d} pennies\n"
                  "   {:d} nickels\n"
                  "   {:d} dimes\n"
                  "   {:d} quarters\n"
                  "   {:d} dollars\n"
                  "   {:d} fives\n"
                  "   {:d} tens\n"
                  "   {:d} twenties\n".format(balance/100,change[0],change[1],change[2],change[3],change[4],change[5],change[6],change[7]))
        state = 0 #reset state to 0, initialization
'''
@file       lab3_UI.py

@brief      User interface for lab 3 that collects data measured on the Nucleo.
@details    This file sets up serial communication between the PC and the 
            Nucleo. The program waits for the user to press the "g" key, and
            once the "g" key has been pressed the Nucleo then waits for the
            user to press the blue input button. When the button is pressed,
            the Nucleo records the voltage change of the button using an 
            analog-to-digital converter and serially sends the measured data to 
            the PC. This program recevies the data, interprets it, plots it, and
            writes the data onto a csv file. 
@author     Michael Yiu
@date       February 4, 2021
'''
import keyboard
import serial
import array
import numpy
import matplotlib.pyplot as plt

## Initializes the serial connection to the Nucleo
ser = serial.Serial(port='COM3',baudrate=115200,timeout=1) 

## Creating an empty time array of type int
time = array.array('i') 
## Creating an empty voltage array of type float
voltage = array.array('f')
## Creating an empty data list
data = []

while True:
    if(keyboard.is_pressed('g')):
        # Do this: if the user hits the g key. It is recommended to hold the 
        # key for about half a second, as tapping the key is sometimes not
        # registered
        ser.write(str('G').encode('ascii')) #Sends G to Nucleo
        print("'g' key pressed, awaiting button press") #Let the user know the g key has been pressed
    ## Reads the line written in Nucleo and removes special characters
    myval = ser.readline().decode('ascii').strip('\r\n').split(',')
    if(myval[0] == 'button pressed'):
        # Do this: if the Nucleo tells the PC that the button has been pressed
        print("Button pressed, collecting data") #Let the user know the button has been pressed
    elif myval[0] != '':
        data.append(myval) #Adds the [time,voltage] list to the data list
        time.append(int(myval[0])) #Adds first value to the time array
        voltage.append(3.3/4095*float(myval[1])) #Adds the second value to the voltage array while converting it from units of ADC counts to volts
        if float(myval[0]) == 5000:
            # Do this: if 5000 microseconds has passed, indicating that the data is done collecting
            print("Data collected and plotted") #Let the user know that the data collection has finished and that a plot of it has been created
            break #Exit the loop to plot
        
plt.plot(time,voltage) #Plots voltage versus time of the button
plt.title('Voltage of the Button versus Time') #Creates the plot title: Voltage of the Button versus Time
plt.xlabel('Time [\u03BCs]') #Creates the x-label: Time in microseconds
plt.ylabel('Voltage [volts]') #Creates the y-label: Voltage in volts
## String that contains a ',' so that when saving the data to a csv file, it will know that values are seperated by commas
delimiter =','
## String that represents the titles for each column in the csv file
header = 'Time (microseconds),Voltage (ADC counts)'
## String that represents the format of the data being saved in the csv file
fmt = '%s'
numpy.savetxt('data.csv',data,delimiter=delimiter,header=header,fmt=fmt) #Saves the data list as a csv, and label the data 
ser.close() #Closes the serial connection to the Nucleo
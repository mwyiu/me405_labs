%% Formatting
clc
clear
format short
syms r_m l_r r_B r_G l_p r_P r_C m_B I_B m_P I_P b g theta_y theta_ydot x x_dot T_x
K = [0 0 0 0] %Temporary value since open-loop and closed-loop block diagrams are on the same simulink
%% Solving for x_doubledot and theta_doubledot with [M]*[x_doubledot; theta_doubledot] = [f]
M = [-(m_B*r_B^2 + m_B*r_C*r_B + I_B)/r_B  -((I_B*r_B + I_P*r_B + m_B*r_B^3 + m_B*r_B*r_C^2 + 2*m_B*r_B^2*r_C + m_P*r_B*r_G^2 + m_B*r_B*x^2)/r_B)
     -(m_B*r_B^2 + I_B)/r_B                -((m_B*r_B^3 + m_B*r_C*r_B^2 + I_B*r_B)/r_B)]
f = [b*theta_ydot - g*m_B*(sin(theta_y)*(r_B + r_C) + x*cos(theta_y)) + (T_x*l_p)/r_m + 2*m_B*theta_ydot*x*x_dot - g*m_P*r_G*sin(theta_y)
     -m_B*r_B*x*theta_ydot^2 - g*m_B*r_B*sin(theta_y)]
xtheta_doubledot = M\f

%% Symbolic Jacobians
J_x = jacobian([xtheta_doubledot(1); xtheta_doubledot(2); x_dot; theta_ydot], [x_dot, theta_ydot, x, theta_y])
J_u = jacobian([xtheta_doubledot(1); xtheta_doubledot(2); x_dot; theta_ydot], T_x)

%% Givens
r_m = 0.0600        %[m]
l_r = 0.0500        %[m]
r_B = 0.0105        %[m]
r_G = 0.0420        %[m]
l_p = 0.1100        %[m]
r_P = 0.0325        %[m]
r_C = 0.0500        %[m]
m_B = 0.0300        %[kg]
m_P = 0.4000        %[kg]
I_B = 0.4*r_B^2*m_B %[kg-m^2]
I_P = 0.00188       %[kg-m^2]
b = 0.0100          %[Nm-s/rad]
g = 9.8067          %[m/s^2]

%% Solving for J_x and J_u by plugging givens and initial conditions  
% Operating Point Conditions
T_x = 0             %[N-m]
x = 0               %[m]
x_dot = 0           %[m/s]
theta_y = 0         %[rad]
theta_ydot = 0      %[rad/s]

A = double(subs(J_x))
B = double(subs(J_u))

%% State A
% Initial Conditions
T_impulse = 0       %[N-m]
T_x = 0             %[N-m]
x = 0               %[m]
x_dot = 0           %[m/s]
theta_y = 0         %[rad]
theta_ydot = 0      %[rad/s]
IC = [x_dot
      theta_ydot
      x
      theta_y]

sim('Lab6_models',1)
figure(1)
subplot(2,2,1)
plot(tout,openloop(:,1))
xlabel('Time [s]')
ylabel('Velocity [m/s]')
title('Velocity vs. Time')
subplot(2,2,2)
plot(tout,openloop(:,2))
xlabel('Time [s]')
ylabel('Angular Velocity [rad/s]')
title('Angular Velocity vs. Time')
subplot(2,2,3)
plot(tout,openloop(:,3))
xlabel('Time [s]')
ylabel('Position [m]')
title('Position vs. Time')
subplot(2,2,4)
plot(tout,openloop(:,4))
xlabel('Time [s]')
ylabel('Angle [rad]')
title('Angle vs. Time')
%% State B
% Initial Conditions
T_impulse = 0       %[N-m]
T_x = 0             %[N-m]
x = 0.05            %[m]
x_dot = 0           %[m/s]
theta_y = 0         %[rad]
theta_ydot = 0      %[rad/s]
IC = [x_dot
      theta_ydot
      x
      theta_y]

sim('Lab6_models',0.4)
figure(2)
subplot(2,2,1)
plot(tout,openloop(:,1))
xlabel('Time [s]')
ylabel('Velocity [m/s]')
axis([0 0.4 -0.05 0.25])
title('Velocity vs. Time')
subplot(2,2,2)
plot(tout,openloop(:,2))
xlabel('Time [s]')
ylabel('Angular Velocity [rad/s]')
title('Angular Velocity vs. Time')
subplot(2,2,3)
plot(tout,openloop(:,3))
xlabel('Time [s]')
ylabel('Position [m]')
title('Position vs. Time')
subplot(2,2,4)
plot(tout,openloop(:,4))
xlabel('Time [s]')
ylabel('Angle [rad]')
title('Angle vs. Time')
%% State C
% Initial Conditions
T_impulse = 0           %[N-m]
T_x = 0                 %[N-m]
x = 0                   %[m]
x_dot = 0               %[m/s]
theta_y = deg2rad(5)    %[rad]
theta_ydot = 0          %[rad/s]
IC = [x_dot
      theta_ydot
      x
      theta_y]

sim('Lab6_models',0.4)
figure(3)
subplot(2,2,1)
plot(tout,openloop(:,1))
xlabel('Time [s]')
ylabel('Velocity [m/s]')
title('Velocity vs. Time')
subplot(2,2,2)
plot(tout,openloop(:,2))
xlabel('Time [s]')
ylabel('Angular Velocity [rad/s]')
title('Angular Velocity vs. Time')
subplot(2,2,3)
plot(tout,openloop(:,3))
xlabel('Time [s]')
ylabel('Position [m]')
title('Position vs. Time')
subplot(2,2,4)
plot(tout,openloop(:,4))
xlabel('Time [s]')
ylabel('Angle [rad]')
title('Angle vs. Time')
%% State D
% Initial Conditions
T_impulse = 1000        %[N-m]
T_x = 0                 %[N-m]
x = 0                   %[m]
x_dot = 0               %[m/s]
theta_y = 0             %[rad]
theta_ydot = 0          %[rad/s]
IC = [x_dot
      theta_ydot
      x
      theta_y]

sim('Lab6_models',0.4)
figure(4)
subplot(2,2,1)
plot(tout,openloop(:,1))
xlabel('Time [s]')
ylabel('Velocity [m/s]')
title('Velocity vs. Time')
subplot(2,2,2)
plot(tout,openloop(:,2))
xlabel('Time [s]')
ylabel('Angular Velocity [rad/s]')
title('Angular Velocity vs. Time')
subplot(2,2,3)
plot(tout,openloop(:,3))
xlabel('Time [s]')
ylabel('Position [m]')
axis([0 0.4 -4 0.5])
title('Position vs. Time')
subplot(2,2,4)
plot(tout,openloop(:,4))
xlabel('Time [s]')
ylabel('Angle [rad]')
title('Angle vs. Time')
%% Closed-Loop Control
% Initial Conditions
K = [-0.05 -0.02 -0.3 -0.2]
T_impulse = 0           %[N-m]
T_x = 0                 %[N-m]
x = 0.05                %[m]
x_dot = 0               %[m/s]
theta_y = 0             %[rad]
theta_ydot = 0          %[rad/s]
IC = [x_dot
      theta_ydot
      x
      theta_y]

sim('Lab6_models',20)
figure(5)
subplot(2,1,1)
plot(tout,closedloop(:,1))
xlabel('Time [s]')
ylabel('Velocity [m/s]')
axis([0 20 -0.15 0.15])
title('Velocity vs. Time')
subplot(2,1,2)
plot(tout,closedloop(:,3))
xlabel('Time [s]')
ylabel('Position [m]')
axis([0 20 -0.05 0.055])
title('Position vs. Time')

figure(6)
subplot(2,1,1)
plot(tout,closedloop(:,2))
xlabel('Time [s]')
ylabel('Angular Velocity [rad/s]')
axis([0 20 -0.25 0.25])
title('Angular Velocity vs. Time')
subplot(2,1,2)
plot(tout,closedloop(:,4))
xlabel('Time [s]')
ylabel('Angle [rad]')
axis([0 20 -0.06 0.075])
title('Angle vs. Time')
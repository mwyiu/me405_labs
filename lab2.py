'''
@file       lab2.py

@brief      Lab 2, Think Fast!
@details    This lab sets up a reflex game that measures the players reaction 
            speed. An LED randomly turns on, and the player hits the blue input
            button to turn off the LED. The reaction time is measured, and when
            the player decides to stop playing, the average reaction time is 
            calculated.
@author     Michael Yiu
@date       January 28, 2021
'''
import pyb
import utime
import random

## State that tracks whether the user has pressed the blue button or not
button_pressed = None

def ButtonPress (which_pin):
    '''
    @brief      The function callback for the external interrupt
    @details    This function is called when the external interrupt is activated.
                This function records the current time on the MCU and changes the
                state of the global state variable, button_pressed, to True.
    @param  which_pin   Which pin is calling this interrupt
    
    '''
    global next_time
    global button_pressed
    # Recording the time at which the button was pressed
    next_time = timer.counter()    
    # Button has been pressed, change state to true
    button_pressed = True
  
## Initializing the green LED on pin A5
pinA5 = pyb.Pin(pyb.Pin.cpu.A5, mode=pyb.Pin.OUT_PP)
## Initializing the blue user input button on pin C13
pinC13 = pyb.Pin(pyb.Pin.cpu.C13, mode=pyb.Pin.IN)
## Initializing timer 2, with a prescaler of 83 so the 84 MHz clock counts in microseconds
timer = pyb.Timer(2, prescaler=83, period=0x7FFFFFFF)

## Initializing the external interrupt on a falling edge of pin C13 
extint = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_UP, callback=ButtonPress)

## Number of runs that has occured
runs = 0
## The total reaction time to be divided by runs later to calculate the average reaction time
reaction_time_total = 0

while True:
    try:
        # Print a start message to the user
        print("Get ready!")
        # Randomly wait either 2 or 3 seconds
        utime.sleep(random.randint(2,3)) 
        # After waiting, turn the LED on
        pinA5.high()
        ## The beginning of time when the LED is turned on
        start_time = timer.counter()
        # Wait for 1 second
        utime.sleep(1)
        if(button_pressed):
            # Do this: if the C13 button has been pressed, and the external interupt has been activated
            ## The time between the time start and when the button has been pressed
            reaction_time = next_time - start_time
            if(reaction_time < 0):
                # Do this: if the C13 button was pressed before the LED turned on
                # Inform the player they pressed the button too early
                print("Too early!\n")
            else:    
                # Record this run and add it to the total number of runs
                runs += 1
                # Add this time to the total reaction time
                reaction_time_total += reaction_time/1e6
                # Inform the player of their reaction time
                print("Your reaction time is: {:} microseconds\n".format(reaction_time))
            # Turn the LED off
            pinA5.low()
            # Button has been released, reset its state 
            button_pressed = False
        else:
            # Do this: if the C13 button was not pressed within the 1 second time frame
            # Turn the LED off
            pinA5.low()
            # Inform the user that they did not react
            print("No reaction time recorded\n")
    except KeyboardInterrupt:
        if(runs == 0):
            # Do this: if the player quits before playing any rounds
            # Print that no runs have been recorded
            print("No runs recorded\n")
        else:
            # Do this: if the player has played at least 1 round
            # Print the number of rounds played and the calculated average reaction time
            print("Over {:} runs, your average reaction time was {:.3f} seconds\n".format(runs,reaction_time_total/runs))
        # Exit loop since the player has decided to stop
        break